#!groovy
import groovy.json.JsonSlurperClassic
import org.apache.commons.lang.StringEscapeUtils
/***********************************************************************************************************************
 * Documentation
 ***********************************************************************************************************************
 *
 * Deployment and branch strategy overview
 * =======================================
 *
 * There are 1 branch within this deployment strategy in use within this pipeline script:
 *
 * - develop branch
 *
 * All new features should be developed on a feature branch with the naming convention "feature/my_new_feature". This
 * will then allow Jenkins to merge the feature branch into develop (test only) and build it against our buildchk org
 * and run some quality checks. The develop branch contains all of the latest updates which are ready for deployment to
 * test & uat. After a successful build to UAT our Buildchk Sandbox is updated with the same code.
 *
 * This Script allows for a daily release of the code (in the develop branch) towards the Validate Sandbox (Check Only).
 * This gives informs us that the code in the develop branch is still deployable to Production at any time. After a
 * successful deployment to Production our Validate Sandbox is updated or refreshed.
 *
 *
 *
 * @author 		Adam Marchbanks (amarchbanks@deloitte.nl)
 * @modified	Thomas Creemers (tcreemers@deloitte.com)
 * @created 	March 2018
 * @copyright 	Deloitte NL. All Rights Reserved.
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Stage definitions
 **********************************************************************************************************************/


node {

	// Job properties
	properties([
			// Discard old logs to ensure we don't consume too much disk space
			// Set numToKeepStr to a higher value if you have sufficient disk space
			// available
			[
					$class  : 'jenkins.model.BuildDiscarderProperty',
					strategy: [
							$class: 'LogRotator',
							numToKeepStr: '10'
					]

			],
			pipelineTriggers([cron('30 7 * * 1-5')])
	])

	// Constants
	def BUILD_NUMBER = env.BUILD_NUMBER
	def SCRATCH_ORG_SFDC_USERNAME
	def SCM_URL
	def SANDBOX_HOST = 'https://test.salesforce.com'
	def PROD_HOST = 'https://login.salesforce.com'
	def UNIT_TESTS_OUTPUT_DIR = 'reports'
	def CHANGE_LOG
	def CHANGED_FILES

	// Set to true if this project is Salesforce DX source code defined. E.g. it is not using the older traditional
	// metadata layout. When this is set to false the script expects your source code to be in the mdapi/ folder.
	def DX_FORMAT_PROJECT = false

	// If not using scratch orgs, do you wish to build against a build check sandbox. Check only deployment.
	def PR_BUILD_CHECK_SANDBOX = true

	// Set the following to true if you wish to update Tracker with the change logs
	def TRACKER_CHANGELOG_NOTIFIER_ENABLED = false

	// Enable BitBucket Pull Request Notifier
	def PR_ENABLE_NOTIFIER = true

	// GIT Provider Credentials - this must be a username/password credential due to at the time of writing no plugin
	// being available for GIT Publisher within Jenkins Pipelines
	def GIT_CRED_ID = 'BitBucket'

	// Validate sandbox credentials
	def VAL_JWT_KEY_CRED_ID = "JWT_KEY_CRED_ID"
	def VAL_CONNECTED_APP_CONSUMER_KEY = ""
	def VAL_HOST = SANDBOX_HOST;
	def VAL_USERNAME = ''
	def VAL_ORGNAME = 'validate'


	// Define the custom tool which points towards the SFDX installation
	// This is here for Linux systems only, Windows must have the SFDX executeable within PATH defined
	def toolbelt = tool 'toolbelt'

	try {

		// Clone the repository
		stage('Clone') {
			// Retrieve source
			checkout scm

			sh 'echo Branch Name: $BRANCH_NAME'

			// Determine the SCRM url so we can push tags later on
			if(isUnix()){
				SCM_URL = sh(returnStdout: true, script: 'git config remote.origin.url').trim()
			} else {
				SCM_URL = bat(returnStdout: true, script: 'git config remote.origin.url').trim()
			}

			// Notify BitBucket pull request that we've started the build
			if(!isDevDeployable() && !isHotfix()) {
				if(PR_ENABLE_NOTIFIER){
					try {
						bitbucketStatusNotify(
								buildState: 'INPROGRESS',
								buildKey: 'build',
								buildName: 'Build'
						)
					} catch(e){
						echo e.message
					}
				}
			}
		}


		// Determines all of the commits since the last production release. This is useful to know to understand what is
		// exactly included in this build when deploying.
		stage('Changes') {
			// Determine the changes
			passedBuilds = []
			lastSuccessfulBuild(passedBuilds, currentBuild);
			CHANGE_LOG = getChangeLog(passedBuilds)

			// Determine which files have been modified
			CHANGED_FILES = getChangedFiles(passedBuilds)

			// Retrieve the changed files using the git tags
			// This is needed as the CHANGED_FILES only

			if(isUnix()){
				def lastTag = sh returnStdout: true, script: 'git describe --match "production/*" --abbrev=0 HEAD 2> /dev/null'
				lastTag = lastTag.trim();
				echo "lastTag: ${lastTag}"
				sh returnStdout: true, script: "git diff --name-only --relative --diff-filter AMR ${lastTag}...HEAD -- mdapi/src > mdapi/changes.txt"
			} else {
				def lastTag = bat returnStdout: true, script: 'git describe --match "production/*" --abbrev=0 HEAD 2> nul'
				lastTag = lastTag.split('\n')[2].trim();
				bat returnStdout: true, script: "git diff --name-only --relative --diff-filter AMR ${lastTag}...HEAD -- mdapi/src > mdapi/changes.txt"
			}

			// Stash the changes
			stash name: 'source', includes: 'mdapi/'
		}


		// Run delta deployments functionality
		if(!DX_FORMAT_PROJECT){
			stage('Delta Deployment') {
				// Ensure we retrieve the changes so we can run the delta deployments safely
				unstash 'source'

				// Run the delta deployments
				def command = 'java -jar build/modules/deltadeployments/deltadeployments.jar -C build/modules/deltadeployments/config.properties -F -L mdapi/changes.txt -P 42.0 -S mdapi/src -T mdapi/target'

				if(isUnix()){
					sh script: command
				} else {
					bat script: command
				}

				// Stash the build source code
				stash name: 'source', includes: 'mdapi/'
			}
		}


		// Run Apex PMD to determine health.
		if(isDevDeployable()) {
			stage('ApexPMD') {
				// Run ApexPMD
				withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
					if(isUnix()){
						sh 'ant -f ./build/modules/pmd/build.xml -q'
					} else {
						bat 'ant -f ./build/modules/pmd/build.xml -q'
					}
				}

				// Publish the results
				step([
						$class: 'hudson.plugins.pmd.PmdPublisher',
						checkstyle: 'build/modules/pmd/report/pmd.xml',
						healthy: '700',	 					// Report health as 100% when the number of warnings is less than this value
						unHealthy: '850',					// Report health as 0% when the number of warnings is greater than this value
						unstableTotalAll: '850',			// annotation threshold
						failedTotalAll: '900',				// annotation threshold
						usePreviousBuildAsReference: true,	// determines whether to always use the previous build as the reference build
				])

				// Manually check the current build result as the error is being reported in another thread
				if(currentBuild.result == 'FAILURE'){
					error 'ApexPMD rule failure'
				}
			}
		}


		// Run QA checks
		if(isDevDeployable()) {
			stage('Daily Validate Check') {

				if(PR_BUILD_CHECK_SANDBOX){
					// Run the QA checks against a traditional sandbox as this project is not set up as a Salesforce DX
					// project
					unstash 'source'

					withCredentials([file(credentialsId: VAL_JWT_KEY_CRED_ID, variable: 'orgSpecificJwtCredId')]) {
						deployMetadata(
								toolbelt,
								VAL_CONNECTED_APP_CONSUMER_KEY,
								VAL_USERNAME,
								orgSpecificJwtCredId,
								VAL_HOST,
								CHANGED_FILES,
								VAL_ORGNAME,
								true
						)
					}
				}
			}
		}


		// Last job for non-deployable pipeline
		if(isDevDeployable()){
			if(PR_ENABLE_NOTIFIER){
				try {
					bitbucketStatusNotify(
							buildState: 'SUCCESSFUL',
							buildKey: 'build',
							buildName: 'Build'
					)
				} catch(n){
					echo n.message
				}
			}
		}

	} catch(e) {
		// Notify BitBucket that the build failed
		if(!isProdDeployable()) {
			if(PR_ENABLE_NOTIFIER){
				try {
					bitbucketStatusNotify(
							buildState: 'FAILED',
							buildKey: 'build',
							buildName: 'Build',
							buildDescription: e.message
					)
				} catch(n){
					echo n.message
				}
			}
		}

		// Notify the developer recipients
		sendStatusEmail('FAILED');

		// Mark the build as failed
		throw e
	}

}


/***********************************************************************************************************************
 * Utility methods
 **********************************************************************************************************************/


/**
 * Here be dragons...!!!
 * JSON objects serialized by JSON Slurper are NOT serializable! This means you must wrap the call to the parseText
 * method within a @NonCPS (native code) function. Not doing this will lead to a cryptic exception during execution
 * indicating a serialization exception.
 */
@NonCPS
def jsonParse(def json) {
	new groovy.json.JsonSlurperClassic().parseText(json)
}


/**
 * Updates the latest commit with a new tag. If the tag already exists on the given commit then it will fail silently.
 * Note: ensure that the GIT username is the form of an email address or it will break this function.
 *
 * @param tag 				Tag to be pushed out to the repository on the commit which is currently being built.
 * @param credentialsId 	The ID of the Jenkins credential which holds the GIT username and password.
 * @param scmUrl  			The GIT repository URL which the tag will be pushed to.
 */
def pushTag(tag, credentialsId, scmUrl){
	echo "tag: ${tag}"
	echo "credentialsId: ${credentialsId}"
	echo "scmUrl: ${scmUrl}"

	withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentialsId, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
		if(isUnix()){
			sh script: "git config --global user.name \"Jenkins\""
			sh script: "git config --global user.email \"${GIT_USERNAME}\""

			scmUrl = scmUrl.drop('https://'.length())

			try {
				sh returnStdout: true, script: "git tag -a ${tag} -m 'Jenkins'"
				sh returnStdout: true, script: "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${scmUrl} --tags"
			} catch(e){
				echo 'Unable to add tag to commit: ' + e.message
			}
		} else {
			bat script: "git config --global user.name \"Jenkins\""
			bat script: "git config --global user.email \"${GIT_USERNAME}\""

			scmUrl = scmUrl.split('\n')[1]
			scmUrl = scmUrl.drop('https://'.length())

			try {
				bat returnStdout: true, script: "git tag -a ${tag} -m 'Jenkins'"
				bat returnStdout: true, script: "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${scmUrl} --tags"
			} catch(e){
				echo 'Unable to add tag to commit: ' + e.message
			}
		}
	}
}


/**
 * Executes given params.task closure and if it fails, asks the user if it should be retried.
 * The task will then be executed again. If the user clicks abort, an exception will be
 * thrown aborting the pipeline.
 *
 * After the task either completes successfully or after the user clicks abort,
 * the params.andFinally closure will be executed.
 *
 * Usage:
 * <pre><code>
 * retry task: {
 *     sh 'something-flaky-generating-html-reports'
 * }, andFinally: {
 *     publishHTML target: [reportDir: 'report', reportFiles: 'index.html', reportName: 'Report']
 * }
 * </pre></code>
 *
 * @params params [task: {}, andFinally: {}]
 */
def retry (params) {
	// waitUntil will retry until the given closure returns true
	waitUntil {
		try {
			// execute the task, if this fails, an exception will be thrown
			// and the params.andFinally() wont be called
			(params.task)()

			// Not all retry calls will have a finally statement, check if one has been provided
			if(params.andFinally) {
				(params.andFinally)()
			}

			// make waitUntil stop retrying this closure
			return true
		} catch(e) {
			try {
				// input asks the user for "Retry? Proceed or Abort". If the
				// user clicks Proceed, input will just return normally.
				// If the user clicks Abort, an exception will be thrown
				input "Retry?"
			} catch (userClickedAbort) {
				// user clicked abort, call the andFinally closure and re-throw the exception
				// to actually abort the pipeline

				// Not all retry calls will have a finally statement, check if one has been provided
				if(params.andFinally) {
					(params.andFinally)()
				}

				// User aborted, throw the exception
				throw userClickedAbort
			}

			// make waitUntil execute this closure again
			return false
		}
	}
}


/**
 * Sends a generic email notification to give an update on the status of the build.
 *
 * @param buildStatus Status of the build which will be included in the email message.
 */
def sendStatusEmail(String buildStatus) {
	// Default values
	def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
	def summary = "${subject} (${env.BUILD_URL})"
	def details = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\n\nCheck console output at: ${env.BUILD_URL} [${env.BUILD_NUMBER}]"

	try {
		emailext (
				subject: subject,
				body: details,
				attachLog: true,
				recipientProviders: [[$class: 'DevelopersRecipientProvider']]
		)
	} catch(e){
		echo 'Unable to send email notification: ' + e.message
	}
}


/**
 * Utility method for determining if the current branch being built can be deployable to develop/Test&UAT. This helps
 * control which aspects of the build must be ran.
 */
def isDevDeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'develop'
}


/**
 * Utility method for determining if the current branch being built can be deployable to Master/Production. This helps
 * control which aspects of the build must be ran.
 */
def isProdDeployable() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME == 'master'
}


/**
 * Utility method for determining if the current branch being built can be deployable to Production, but is a hotifx
 * and must instead take an alternative route to Production.
 */
def isHotfix() {
	echo 'Branch name: ' + env.BRANCH_NAME
	return env.BRANCH_NAME.length() > 7 && env.BRANCH_NAME.substring(0, 7) == 'hotfix/'
}


/**
 * Finds all of the past builds which have not been successful before the current build. This method will
 * recursively call itself and will move to the previous build until it finds one which successful or until
 * it reaches no other build.
 *
 * @param passedBuilds 	A list passed by reference which will be populated with the build numbers which
 * 						have not been successful before the current build.
 * @param build  		Current build being checked.
 */
def lastSuccessfulBuild(passedBuilds, build) {
	if ((build != null) && (build.result != 'SUCCESS')) {
		passedBuilds.add(build)
		lastSuccessfulBuild(passedBuilds, build.getPreviousBuild())
	}
}


/**
 * Using the build references passed into this method, it will build a list of all of the changes found in
 * all of them and return a string.
 */
@NonCPS
def getChangeLog(passedBuilds) {
	def log = ""

	for (int x = 0; x < passedBuilds.size(); x++) {
		def currentBuild = passedBuilds[x];
		def changeLogSets = currentBuild.rawBuild.changeSets

		for (int i = 0; i < changeLogSets.size(); i++) {
			def entries = changeLogSets[i].items
			for (int j = 0; j < entries.length; j++) {
				def entry = entries[j]
				log += "${entry.msg} by ${entry.author}\n"

				def files = new ArrayList(entry.affectedFiles)
				for (int k = 0; k < files.size(); k++) {
					def file = files[k]
				}
			}
		}
	}

	return log
}


/**
 * Using the build references passed into this method, it will build a list of all of the changes found in
 * all of them and return a list containing the filenames which were modified.
 */
@NonCPS
def getChangedFiles(passedBuilds) {
	def changes = []

	for (int x = 0; x < passedBuilds.size(); x++) {
		def currentBuild = passedBuilds[x];
		def changeLogSets = currentBuild.rawBuild.changeSets

		for (int i = 0; i < changeLogSets.size(); i++) {
			def entries = changeLogSets[i].items
			for (int j = 0; j < entries.length; j++) {
				def entry = entries[j]

				def files = new ArrayList(entry.affectedFiles)
				for (int k = 0; k < files.size(); k++) {
					def file = files[k]
					changes.add("${file.path}")
				}
			}
		}
	}

	changes.unique { a, b -> a <=> b }
	return changes
}


/***********************************************************************************************************************
 * Stage specific methods
 **********************************************************************************************************************/

/**
 * Deploys traditional metadata files to a conventional sandbox.
 *
 * @param toolbelt 		Path to where the SFDX tooling is installed.
 * @param clientId 		The Connected App client ID which will be used for logging into that sandbox.
 * @param jwtkeyfile 	The path to the certificate which will be used to sign the JWT token when logging into the sandbox.
 * @param instanceurl  	The host name to use to log into Salesforce (e.g. https://login.salesforce.com)
 * @param changedFiles  List of changed files
 * @param orgName  		Name of the sandbox or Production environment
 * @param checkOnly     Set to true if the deployment should be check only deployment
 */
def deployMetadata(toolbelt, clientId, username, jwtkeyfile, instanceurl, changedFiles, orgName, checkOnly) {
	// Login into the sandbox
	if(isUnix()){
		rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	} else {
		rc = bat returnStatus: true, script: "sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	}

	if (rc != 0) {
		error 'Failed to login into the org'
	}

	// Run the Flow Repo Handler
	runTheFlowRepoHandler(toolbelt, username);

	// Modify the profiles
	modifyProfiles()

	// Modify outbound messages
	runOutboundMessageModifier(orgName)

	// Modify remote site security
	runRemoteSiteSecurityModifier(orgName)

	// Run anonymous Apex against this org
	if(!checkOnly) {
		executeAnonApex(toolbelt, username, changedFiles, 'pre')
	}

	// Deploy the converted code
	def checkOnlyArg = checkOnly? '--checkonly' : ''

	if(isUnix()){
		rmsg = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:mdapi:deploy --targetusername ${username} --deploydir mdapi/target/src --testlevel RunLocalTests --json ${checkOnlyArg} --ignorewarnings"
	} else {
		rmsg = bat returnStdout: true, returnStatus: false, script: "sfdx force:mdapi:deploy --targetusername ${username} --deploydir mdapi/target/src --testlevel RunLocalTests --json ${checkOnlyArg} --ignorewarnings"
		rmsg = rmsg.split('\n')[2].trim()
	}


	def robj = jsonParse("${rmsg}")
	def deploymentId = robj.result.id;

	// Update the user with the progress of the deployment
	def checkStatus;

	if(isUnix()){
		checkStatus = sh returnStatus: true, script: "${toolbelt}/sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
	} else {
		checkStatus = bat returnStatus: true, script: "sfdx force:mdapi:deploy:report --targetusername ${username} -i ${deploymentId} --wait 30"
	}

	if(checkStatus != 0){
		error 'Failed to retrieve status of deployment'
	}

	// Retrieve the accurate overall code coverage
	def orgDetails;

	if(isUnix()){
		orgDetails = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:org:display --targetusername ${username} --json"
	} else {
		orgDetails = bat returnStdout: true, returnStatus: false, script: "sfdx force:org:display --targetusername ${username} --json"
		orgDetails = orgDetails.split('\n')[2].trim()
	}

	def org = jsonParse("${orgDetails}")
	def accessToken = org.result.accessToken
	def instanceUrl = org.result.instanceUrl

	withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh "ant -f ./build/modules/codecoverage/build.xml -DdeploymentId=${deploymentId} -Dsf.sessionId=${accessToken} -Dsf.endpointUrl=${instanceUrl} -q"
		} else {
			bat "ant -f ./build/modules/codecoverage/build.xml -DdeploymentId=${deploymentId} -Dsf.sessionId=${accessToken} -Dsf.endpointUrl=${instanceUrl} -q"
		}
	}


	// Publish the test results to Jenkins
	def coverageReport = "reports/TEST-Apex.xml";
	if (fileExists(coverageReport)) {
		junit keepLongStdio: true, allowEmptyResults: true, testResults: coverageReport
	}
}


/**
 * Runs the flow repo handler.
 *
 * @param  toolbelt 		Path to where the SFDX tooling is installed.
 * @param  username  		The target username to use to run the anonymous apex against a specified org.
 */
def runTheFlowRepoHandler(toolbelt, username){
	timeout(time: 10, unit: 'MINUTES') {
		withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
			if(isUnix()){
				sh "ant -f ./build/modules/flowrepohandler/build.xml -Dscript.sfdx=${toolbelt} -Dsf.username=${username.trim()} -q"
			} else {
				bat "ant -f ./build/modules/flowrepohandler/build.xml -Dsf.username=${username.trim()} -q"
			}
		}
	}
}


/**
 * Removes hard to handle aspects from profiles.
 */
def modifyProfiles(){
	def command = "ant -f ./build/modules/profilemod/build.xml"

	withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	}
}


/**
 * Modifies the outbound message endpoints to be org-specific.
 */
def runOutboundMessageModifier(orgName){
	def command = "ant -f ./build/modules/outboundmessages/build.xml -Dsf.org=${orgName}"

	withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	}
}


/**
 * Modifies the remote site security settings to be org-specific.
 */
def runRemoteSiteSecurityModifier(orgName){
	def command = "ant -f ./build/modules/remotesitesecurity/build.xml -Dsf.org=${orgName}"

	withEnv(["PATH=${tool 'Ant'}/bin:${env.PATH}"]) {
		if(isUnix()){
			sh command
		} else {
			bat command
		}
	}
}


/**
 * Executes anonymous Apex against an org using the changed files for either pre/post deployment
 * steps.
 *
 * @param  toolbelt 		Path to where the SFDX tooling is installed.
 * @param  username  		The target username to use to run the anonymous apex against a specified org.
 * @param  changedFiles 	List of changed files to use to find the anonymous apex to execute
 * @param  prefix  			The file prefix to use to find the anonymous apex files. Valid values are
 *                    		"pre" or "post".
 */
def executeAnonApex(toolbelt, username, changedFiles, prefix){
	// Run anonymous Apex against this org
	echo 'Running anonymous Apex'

	if(changedFiles != null) {
		for (int i = 0; i < changedFiles.size(); i++) {
			def file = changedFiles[i]

			// Skip the file if the filename is obviously too short
			if (file.length() <= 12) continue;

			if(fileExists(file)) {
				if (file.substring(0, 8) == 'scripts/' && (file.substring(8, 11) == prefix || file.substring(8, 12) == prefix)) {
					echo "Executing: ${file}"

					if (isUnix()) {
						sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:apex:execute --targetusername ${username} --apexcodefile ${file} --json --loglevel debug"
					} else {
						bat returnStdout: true, returnStatus: false, script: "sfdx force:apex:execute --targetusername ${username} --apexcodefile ${file} --json --loglevel debug"
					}
				}
			}
		}
	}
}


/**
 * This will update Tracker with the complete change log of changes in the pipeline since the last Production
 * deployment for the org which is being deployed to. This is useful to provide business with an insight into where
 * updates are currently deployed to.
 */
def updateTrackerSCMChangelog(toolbelt, clientId, username, jwtkeyfile, instanceurl, changelog, orgName) {
	// Login into the sandbox
	if(isUnix()){
		rc = sh returnStatus: true, script: "${toolbelt}/sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	} else {
		rc = bat returnStatus: true, script: "sfdx force:auth:jwt:grant --clientid ${clientId} --username ${username} --jwtkeyfile ${jwtkeyfile} --instanceurl ${instanceurl} --loglevel debug"
	}

	if (rc != 0) {
		error 'Failed to login into the org'
	}

	// Deploy the converted code
	changelog = StringEscapeUtils.escapeHtml(changelog)
	echo "Changelog: ${changelog}"
	def lines = changelog.split('\n');
	def log = '<ul>';

	// Fix for maximum command line length for windows
	int numLines = !isUnix() && lines.size() > 40? 40 : lines.size();

	for(int i=0; i < numLines; i++){
		log += "<li>${lines[i]}</li>"
	}

	if(numLines == 40){
		log += "<li>Change log too large.</li>"
	}

	log += '</ul>'
	log = log.replace('\n', '').replace('\r', '')

	if(isUnix()){
		rmsg = sh returnStdout: true, returnStatus: false, script: "${toolbelt}/sfdx force:data:record:create -s SCM_Change_Log__c -v \"Org__c=${orgName} Changelog__c='${log}'\" --targetusername ${username}"
	} else {
		rmsg = bat returnStdout: true, returnStatus: false, script: "sfdx force:data:record:create -s SCM_Change_Log__c -v \"Org__c=${orgName} Changelog__c='${log}'\" --targetusername ${username}"
	}

	echo 'Publishing changelog result: ' + rmsg
}
